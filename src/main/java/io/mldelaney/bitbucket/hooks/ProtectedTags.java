package io.mldelaney.bitbucket.hooks;

import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.atlassian.bitbucket.repository.RefChangeType;

import com.atlassian.plugin.spring.scanner.annotation.imports.BitbucketImport;

import com.atlassian.bitbucket.event.tag.TagDeletionHookRequest;
import com.atlassian.bitbucket.hook.repository.PreRepositoryHook;
import com.atlassian.bitbucket.hook.repository.PreRepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.RepositoryHookResult;
import com.atlassian.bitbucket.i18n.I18nService;

import io.mldelaney.bitbucket.utils.SemanticVersion;

/**
 *
 */
class ProtectedTags implements PreRepositoryHook<TagDeletionHookRequest> {
  /**
   *
   */
  private static final Logger log = LoggerFactory.getLogger(ProtectedTags.class);

  /**
   * The prefix attached to GIT Tags
   *
   */
  private static final String TAGS_PREFIX = "refs/tags";

  /**
   * Character to split the git refs on.
   *
   */
  private static final String REFS_SEPARATOR = "/";

  /**
   * Internationalize Service helper.
   */
  private final I18nService i18nService;

  /**
   * Autowire constructor for Bitbucket.
   */
  public ProtectedTags(@BitbucketImport I18nService i18nService) {
    this.i18nService = i18nService;
  }

  @Override
  public RepositoryHookResult preUpdate(PreRepositoryHookContext context, TagDeletionHookRequest request) {
    // Find all refs that are about to be deleted
    Set<String> deletedRefIds = request.getRefChanges().stream()
            .filter(refChange -> refChange.getType() == RefChangeType.DELETE)
            .map(refChange -> refChange.getRef().getId())
            .collect(Collectors.toSet());

    // nothing is going to be deleted, no problem
    if (deletedRefIds.isEmpty()) {
        return RepositoryHookResult.accepted();
    }

    for(String ref: deletedRefIds) {
      log.debug("Checking ref id {0}", ref);

      if (!ref.startsWith(TAGS_PREFIX)) {
        continue;
      }

      String[] parts = ref.split(REFS_SEPARATOR);
      String tag = parts[parts.length - 1];

      if (SemanticVersion.isValid(tag)) {
        return RepositoryHookResult.rejected(
            i18nService.getMessage("hook.guide.branchinreview.summary"),
            i18nService.getMessage("hook.guide.branchinreview.details", tag));
      }
    }

    return RepositoryHookResult.accepted();
  }
}
