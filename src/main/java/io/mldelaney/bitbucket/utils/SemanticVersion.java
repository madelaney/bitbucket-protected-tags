package io.mldelaney.bitbucket.utils;

public class SemanticVersion {
  /**
   * The Regular Expression to find Semantic Version tags
   *
   */
  private static final String REGEX = "^v?([0-9]{1,}(\\.[0-9]{1,}){1,})(\\-[A-Z0-9]+(\\+[0-9A-Za-z]+)?)?$";

  /**
   * Checks to see if this String matches a known regular expression pattern.
   *
   * @param value
   * @return
   */
  static public boolean isValid(final String value) {

    return value.trim().matches(REGEX);
  }
}
