package io.mldelaney.bitbucket.api;

public interface MyPluginComponent {
  String getName();
}
