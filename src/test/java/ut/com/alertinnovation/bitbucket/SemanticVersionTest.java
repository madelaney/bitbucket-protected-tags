package ut.io.mldelaney.bitbucket;

import org.junit.Test;

import io.mldelaney.bitbucket.utils.SemanticVersion;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;

public class SemanticVersionTest {
  @Test
  public void testVersionWithWhiteSpace() {
    assertTrue(SemanticVersion.isValid("    v1.0.0    "));
    assertTrue(SemanticVersion.isValid("    v1.0.0"));
    assertTrue(SemanticVersion.isValid("v1.0.0    "));
  }

  @Test
  public void testInvalidSingleDigitVersion() {
    assertFalse(SemanticVersion.isValid("v1"));
    assertFalse(SemanticVersion.isValid("1"));
  }

  @Test
  public void testThreeDigitVersionWithLeading() {
    assertTrue(SemanticVersion.isValid("v1.0.0"));
    assertFalse(SemanticVersion.isValid("Version"));
  }

  @Test
  public void testThreeDigitVersionWithoutLeading() {
    assertTrue(SemanticVersion.isValid("1.0.0"));
  }

  @Test
  public void testThreeDigitWithClassifier() {
    assertTrue(SemanticVersion.isValid("1.0.0-BETA1"));
    assertTrue(SemanticVersion.isValid("1.0.0-BETA"));

    /**
     * A classifier must be a fully qualified string, it cannot just be
     * a hyphen (-), if it is, we should not see this as a valid
     * semantic version.
     *
     */
    assertFalse(SemanticVersion.isValid("1.0.0-"));
  }

  @Test
  @Ignore
  public void testThreeDigitWithClassifierMeta() {
    assertTrue(SemanticVersion.isValid("1.0.0-BETA1+FEATURE"));
    assertTrue(SemanticVersion.isValid("1.0.0-BETA+FEATURE"));

    /**
     * Metadata must be a fully qualified string, it cannot just be a
     * plus (+) sign, if it is, we should not see this as a valid
     * semantic version.
     *
     */
    assertFalse(SemanticVersion.isValid("1.0.0-BETA+"));
  }
}
